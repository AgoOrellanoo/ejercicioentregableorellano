﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lógica
{
    class Siniestro
    {
       public DateTime FechaSiniestro { get; set; }
       public int PolizaAsociada { get; set; }
       public Persona ClienteInformador { get; set; }

        public Siniestro(DateTime fecha, int poliza, Persona cliente)
        {
            FechaSiniestro = fecha;
            PolizaAsociada = poliza;
            ClienteInformador = cliente;
        }

    }
}
