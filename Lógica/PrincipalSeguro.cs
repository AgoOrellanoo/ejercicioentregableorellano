﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lógica
{
    public class PrincipalSeguro
    {
        List<PrincipalSeguro> ListaSeguro = new List<PrincipalSeguro>();
        List<Siniestro> ListaSiniestro = new List<Siniestro>();
        List<SiniestroAsociado> ListaNuevoSiniestro = new List<SiniestroAsociado>();

        public Persona Tomador { get; set; }
        public Persona Beneficiario { get; set; }
        public double PrimaAsegurada { get; set; }
        public int NroDePoliza { get; set; }
        public double MontoMensual { get; set; }

        public double PorcentajeCobertura()
        {
            double anual = MontoMensual * 12;
            return (PrimaAsegurada / anual) * 100;
        }
        public bool RegistroSiniestros(int dni, DateTime fechaSiniestro, int nroPoliza)
        {
            foreach (var item in ListaSeguro)
            {
                if (item.NroDePoliza == NroDePoliza)
                {
                    if (item is SeguroIncendio)
                    {
                        //CORRECCIÓN: ESTO NO DEBERIA SER ASI, NECESITAS HACER UN MÉTODO BASE ABSTRACTO EN SEGURO QUE SE LLAME POR EJ: EsSeguroValido() Y QUE RETORNE UN VERDADERO O FALSO SEGUN LA IMPLEMENTACIÓN EN CADA TIPO DE SEGURO (QUE CADA UNO DEBERIA SABER QUE VALIDACIONES AHCER)
                        //DE ESTA FORMA SOLO TENDRÍAS QUE HACER item.EsSeguroValido() Y LISTO, TE AHORRAS TODOS ESTOS IF.
                        //DE PASO CUANDO HACES ESTA TRANSAFORMACIÓN, SIEMPRE TENES QUE DCONTROLAR QUE SEGURO NO SEA NULL, PORQUE SI NO ES SEGURO INCENDIO, NO TIENE LAS PROPIEDADES POR LAS QUE CONTROLAS (PERO ACORDATE LO DE ANTES, NO DEBERIAS RESOLVER ESTO ASI.)
                        SeguroIncendio seguro = item as SeguroIncendio;
                        if (item.Tomador.Dni == dni && seguro.CantidadMatafuegos >= seguro.CantidadEnchufes)
                        {
                            if (seguro.ViviendaOTrabajo == "Vivienda" && seguro.MetrosCuadrados < 400)
                            {
                                return false;
                            }
                            else
                            {
                                Siniestro listaSiniestro = new Siniestro(fechaSiniestro, nroPoliza, item.Tomador);
                                ListaSiniestro.Add(listaSiniestro);
                                return true;

                            }
                        }
                    }
                    else
                    {
                        if (item is SeguroRoboDaño)
                        {
                            SeguroRoboDaño seguro = new SeguroRoboDaño();
                            if (seguro.Beneficiario.Dni == dni && seguro.Tomador.Dni == dni && seguro.TamañoPulgada < 6)
                            {
                                Siniestro listaSiniestros = new Siniestro(fechaSiniestro, nroPoliza, item.Tomador);
                                ListaSiniestro.Add(listaSiniestros);
                                return true;
                            }
                        }
                    }
                }
            }
            return false;
        }
        public void Almacenar(int dni)
        {
            foreach (Siniestro item in ListaSiniestro)
            {
                if (item.ClienteInformador.Dni == dni)
                {
                    SiniestroAsociado nuevo = new SiniestroAsociado();
                    Persona nuevos = new Persona();
                    ListaNuevoSiniestro.Add(new SiniestroAsociado() { NombreYApellido = nuevo.NombreYApellido });
                    ListaNuevoSiniestro.Add(new SiniestroAsociado() { Fecha = item.FechaSiniestro });

                    foreach (PrincipalSeguro item2 in ListaSeguro)
                    {
                        if (item2 is SeguroIncendio)
                        {
                            ListaNuevoSiniestro.Add(new SiniestroAsociado() { TipoDeSeguro = "Incendio." });
                        }
                        else
                        {
                            if (item2 is SeguroAutomotoress)
                            {
                                ListaNuevoSiniestro.Add(new SiniestroAsociado() { TipoDeSeguro = "Automotor." });
                            }
                            else
                            {
                                if (item2 is SeguroRoboDaño)
                                {
                                    ListaNuevoSiniestro.Add(new SiniestroAsociado() { TipoDeSeguro = "Robo." });
                                }
                            }
                        }
                    }
                }

            }

        }
    }



}
