﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lógica
{
    class SeguroIncendio:PrincipalSeguro
    {
        public double MetrosCuadrados { get; set; }
        public int CantidadMatafuegos { get; set; }
        public int CantidadEnchufes { get; set; }
        public string ViviendaOTrabajo { get; set; }
        public double MontoBienesMateriales { get; set; }

    }
}
