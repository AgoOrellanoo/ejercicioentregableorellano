﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lógica
{
    class SeguroAutomotoress:PrincipalSeguro
    {
        public int Patente { get; set; }
        public string Marca { get; set; }
        public int AñoFabricacion { get; set; }
        public string TipoVehiculo { get; set; }
        public int MaximoOcupantes { get; set; }

    }
}
