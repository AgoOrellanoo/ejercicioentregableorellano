﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lógica
{
    class SiniestroAsociado
    {
        public string NombreYApellido { get; set; }

        public DateTime Fecha { get; set; }

        public string TipoDeSeguro { get; set; }
    }
}
