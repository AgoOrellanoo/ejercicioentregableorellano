﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lógica
{
    class SeguroRoboDaño:PrincipalSeguro
    {
        public string ModeloTelefono { get; set; }
        public double TamañoPulgada { get; set; }
        public double PrecioActual { get; set; }
        public DateTime FechaCompra { get; set; }

    }
}
